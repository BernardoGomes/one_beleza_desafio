# Desafio da tela de agendamentos

Neste problema você deve implementar a tela de agendamento de clientes do app One Beleza e Bem-Estar. Este desenvolvimento engloba a tela com uma lista exibindo algumas informações. Caso algum item seja clicado será exibido duas opções Editar e Cancelar.
Nenhum botão ou opção precisar possuir a funcionalidade implementada.

A interface em anexo precisa ser implementada consumindo os dados que estão no arquivo retornoDaRequisicao.  A requisição do arquivo tem uma lista com três objetos de retorno. Essa quantidade não é fixa, varia com a quantidade de agendamentos do cliente.

Obs: No lado esquerdo existe um background que está em cor laranja.  Sua cor é definido pela variavel letraStatus. M: "Laranja", A: Azul", P: "Verde"

O teste será efetuado com retorno com quantidades diversas.


***Restrições***
* Deve ser utilizado exclusivamente o IONIC.
* É necessário que seu código execute no Windows 8 OU no Lubuntu;
* Ao finalizar, envie uma uma pasta compactada contendo todo o código e dependências.
* Uma rota somente para página inicial.
* Eu vou executar os seguintes passos:


>1. extrair sua pasta zipada
2. *cd sua-pasta
3. ionic serve

***Artefatos***
* Imagens e retornoDaRequisicao estão na pasta arquivos
